﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Configuration;
using System.Diagnostics;
using System.Windows.Navigation;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace LastOasisTrader
{
    public partial class MainWindow : Window
    {
        string mDbPath;
        SQLiteConnection mConn;
        SQLiteDataAdapter mAdapter;
        DataTable mTable;
        SQLiteCommand mCmd;
        string EditProductid, EditProduct, EditPrice, EditQuantity, EditServer;
        string item_id, item_product, item_sell, item_quantity, item_total, item_server;
        string Server_name, Server_id;
        double FullPrice;
        object item;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            LoadProductData();
            PopulateProductList();
            PopulateServerList();
            LoadItemsData();
            LoadServerData();
        }

        #region populateList
        private void PopulateProductList()
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            try
            {
                var con = new SQLiteConnection(mDbPath);
                con.Open();
                string stm = "SELECT * FROM tbl_category order by id";
                var cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                ObservableCollection<string> list = new ObservableCollection<string>();
                while (rdr.Read())
                {
                    list.Add(rdr.GetInt32(0)+"¦"+rdr.GetString(1) + "¦" + rdr.GetString(2) + "¦" + rdr.GetString(3) + "¦" + rdr.GetString(4));
                }
                this.ProductsList.ItemsSource = list;
                this.ProductsList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Error Message : " + ex);
            }
        }
        private void PopulateServerList()
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            try
            {
                var con = new SQLiteConnection(mDbPath);
                con.Open();
                string stm = "SELECT * FROM tbl_server order by id";
                var cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                ObservableCollection<string> list = new ObservableCollection<string>();
                while (rdr.Read())
                {
                    list.Add(rdr.GetString(1));
                }
                this.ServerList.ItemsSource = list;
                this.ServerList.SelectedIndex = 0;
                this.ServerList_item.ItemsSource = list;
                this.ServerList_item.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Error Message : " + ex);
            }
        }
        private void ServerListItem_changer(object sender, SelectionChangedEventArgs e)
        {
            item_server = ServerList_item.SelectedItem.ToString();
        }

        private void ServerList_change(object sender, SelectionChangedEventArgs e)
        {
            EditServer = ServerList.SelectedItem.ToString();
        }

        private void ProductList_change(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string product_selected = ProductsList.SelectedItem.ToString();
                string[] words = product_selected.Split('¦');

                item_product = words[1];
                item_server = words[4];
                item_quantity = words[3];

            }
            catch(Exception ex) { }
            

        }

        #endregion

        #region Products
        private void Button_Submit_Category(object sender, RoutedEventArgs e)
        {
            if (txtproduct.Text != "" && txtprice.Text != "" && txtquantity.Text != "" && EditServer != "") {
                mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
                try
                {
                    using (mConn = new SQLiteConnection(mDbPath))
                    {
                        mConn.Open();
                        mCmd = new SQLiteCommand("insert into tbl_category (Product, Price, Quantity, Server) values(@Product, @Price, @Quantity, @Server)", mConn);
                        mCmd.Parameters.AddWithValue("@Product", txtproduct.Text);
                        mCmd.Parameters.AddWithValue("@Price", txtprice.Text.Replace(".",","));

                        


                        mCmd.Parameters.AddWithValue("@Quantity", txtquantity.Text);
                        mCmd.Parameters.AddWithValue("@Server", EditServer);
                        mCmd.Prepare();
                        mCmd.ExecuteNonQuery();
                        LoadProductData();
                    }
                    PopulateProductList();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Error Message : " + ex);
                }
                txtproduct.Text = String.Empty;
                txtprice.Text = String.Empty;
                txtquantity.Text = String.Empty;
            }
        }
        private void Button_Delete_CategoryElement_Click(object sender, RoutedEventArgs e)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            try
            {
                if (EditProductid != null)
                {
                    using (mConn = new SQLiteConnection(mDbPath))
                    {
                        mConn.Open();
                        mCmd = new SQLiteCommand("delete from tbl_category where id = " + Int32.Parse(EditProductid), mConn);
                        mCmd.ExecuteNonQuery();
                        LoadProductData();
                    }
                    PopulateProductList();
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Error Message : " + ex);
            }
        }
        private void ButtonEdit_Update_Category(object sender, RoutedEventArgs e)
        {
            if (EditProductid != null)
            {
                if (!txtproduct.Text.Equals(EditProduct) || !txtprice.Text.Equals(EditPrice) || !txtquantity.Text.Equals(EditQuantity) || !EditServer.Equals(EditServer))
                {
                    try
                    {
                        using (mConn = new SQLiteConnection(mDbPath))
                        {
                            mConn.Open();
                            mCmd = new SQLiteCommand("update tbl_category SET Product = @Product, Price = @Price, Quantity = @Quantity, Server = @Server where id =" + Int32.Parse(EditProductid), mConn);
                            mCmd.Parameters.AddWithValue("@Product", txtproduct.Text);
                            mCmd.Parameters.AddWithValue("@Price", txtprice.Text.Replace(".", ","));
                            mCmd.Parameters.AddWithValue("@Quantity", txtquantity.Text);
                            mCmd.Parameters.AddWithValue("@Server", EditServer);

                            mCmd.ExecuteNonQuery();
                            LoadProductData();
                        }
                        PopulateProductList();
                    }
                    catch (Exception ex)
                    {
                       // MessageBox.Show("Error Message : " + ex);
                    }
                }
                else
                {
                   // MessageBox.Show("No Entry Changed.");
                }
            }
        }
        private void LoadProductData()
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            using (mConn = new SQLiteConnection(mDbPath))
            {
                mConn.Open();
                mCmd = null;
                mCmd = new SQLiteCommand("SELECT * FROM tbl_category order by id", mConn);
                mCmd.ExecuteNonQuery();
                mAdapter = new SQLiteDataAdapter(mCmd);
                mTable = new DataTable("tbl_category");
                mAdapter.Fill(mTable);
                mConn.Close();
                categoryDataGridatDatabase.ItemsSource = mTable.DefaultView;
                mAdapter.Update(mTable);
                this.categoryDataGridatDatabase.Columns[0].Header = "ID";
                this.categoryDataGridatDatabase.Columns[1].Header = "PRODUCT";
                this.categoryDataGridatDatabase.Columns[2].Header = "BUY PRICE";
                this.categoryDataGridatDatabase.Columns[3].Header = "QUANTITY";
                this.categoryDataGridatDatabase.Columns[4].Header = "SERVER";
            }
        }


        private string[] GetPrice(string product)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            double price = 0;
            double count = 0;
            string[] output = new string[2];
            try
            {
                var con = new SQLiteConnection(mDbPath);
                con.Open();
                string stm = "SELECT * FROM tbl_item where product='"+ product.ToString()+"'" ;
                var cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                ObservableCollection<string> list = new ObservableCollection<string>();
                while (rdr.Read())
                {
                    price += Convert.ToDouble(rdr.GetString(4));
                    count += Convert.ToDouble(rdr.GetString(3));
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Error Message : " + ex);
            }

            output[0] = price.ToString();
            output[1] = count.ToString();

            return output;
        }
        
        private string GetTotalGain(string product,string quantity)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            double price = 0;
            try
            {
                var con = new SQLiteConnection(mDbPath);
                con.Open();
                string stm = "SELECT * FROM tbl_category where product='" + product.ToString() + "'";
                var cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                ObservableCollection<string> list = new ObservableCollection<string>();
                while (rdr.Read())
                {
                    price += Convert.ToDouble(rdr.GetString(2));
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error Message : " + ex);
            }
            price = price* Convert.ToDouble(quantity);
            return price.ToString();
        }
        
        private void categoryDataGridatDatabase_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.categoryDataGridatDatabase.SelectedItem != null)
                {
                    item = this.categoryDataGridatDatabase.SelectedItem;
                    EditProductid = (this.categoryDataGridatDatabase.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text;
                    EditProduct = (this.categoryDataGridatDatabase.SelectedCells[1].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtproduct.Text = EditProduct;

                    EditPrice = (this.categoryDataGridatDatabase.SelectedCells[2].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtprice.Text = EditPrice;

                    EditQuantity = (this.categoryDataGridatDatabase.SelectedCells[3].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtquantity.Text = EditQuantity;

                    EditServer = (this.categoryDataGridatDatabase.SelectedCells[4].Column.GetCellContent(item) as TextBlock).Text;

                    FullPrice = Convert.ToDouble(EditPrice.ToString()) * Convert.ToDouble(EditQuantity.ToString());

                    FullGain.Content = FullPrice.ToString() + " FLOTS";

                    string item_price = GetPrice(EditProduct)[0];
                    string item_Count = GetPrice(EditProduct)[1];
                    
                    if (Convert.ToDouble(item_Count) > Convert.ToDouble(EditQuantity))
                    {
                        item_Count = EditQuantity;
                    }

                    FlotUsed.Content = item_price + " FLOTS"; ;

                    //string quantity = GetQuantityProductsItem(EditProduct);
                    
                    string gain = GetTotalGain(EditProduct, item_Count);

                   

                    double TotalGain = 0;
                    if (gain != "0")
                    {
                         TotalGain = Convert.ToDouble(gain) - Convert.ToDouble(item_price);
                    }

                    ReselerGain.Foreground = new System.Windows.Media.SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF41D30B"));

                    if (TotalGain >= FullPrice)
                    {
                        TotalGain = FullPrice;
                    }

                    if (TotalGain <0) {
                        ReselerGain.Foreground = new SolidColorBrush(Colors.Red);
                    }

                    // |116634 - 38762|/116634 = 77872/116634 = 0.66766123085893 = 66.766123085893%
                    double percent = ((TotalGain - Convert.ToDouble(item_price)) / TotalGain) * 100;

                    ReselerGain.Content = TotalGain.ToString() + " FLOTS ( "+ Math.Round(percent, 2) + "% )"; 



                    ServerList.SelectedItem = EditServer;

                }
            }
            catch (Exception exp)
            {
               // MessageBox.Show("Message:" + exp);
            }
        }

        #endregion

        #region Items



        private void LoadItemsData()
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            using (mConn = new SQLiteConnection(mDbPath))
            {
                mConn.Open();
                mCmd = null;
                mCmd = new SQLiteCommand("SELECT * FROM tbl_item order by id", mConn);
                mCmd.ExecuteNonQuery();
                mAdapter = new SQLiteDataAdapter(mCmd);
                mTable = new DataTable("tbl_item");
                mAdapter.Fill(mTable);
                mConn.Close();
                itemsDataGridatDatabase.ItemsSource = mTable.DefaultView;
                mAdapter.Update(mTable);
                this.itemsDataGridatDatabase.Columns[0].Header = "ID";
                this.itemsDataGridatDatabase.Columns[1].Header = "PRODUCT";
                this.itemsDataGridatDatabase.Columns[2].Header = "SELL PRICE";
                this.itemsDataGridatDatabase.Columns[3].Header = "QUANTITY";
                this.itemsDataGridatDatabase.Columns[4].Header = "FLOTS";
                this.itemsDataGridatDatabase.Columns[5].Header = "SERVER";
            }
        }

        private string GetTotalQuantityProduct(string product)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            double price = 0;
            try
            {
                var con = new SQLiteConnection(mDbPath);
                con.Open();
                string stm = "SELECT * FROM tbl_category where product='" + product.ToString() + "'";
                var cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                ObservableCollection<string> list = new ObservableCollection<string>();
                while (rdr.Read())
                {
                    price += Convert.ToDouble(rdr.GetString(3));
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error Message : " + ex);
            }

            return price.ToString();
        }

        private void Button_Add_ItemElement_Click(object sender, RoutedEventArgs e)
        {
            if (item_server != "" && item_product != "" && txtsellprice.Text != "" && txtquantitysell.Text != "")
            {
                mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
                try
                {
                    using (mConn = new SQLiteConnection(mDbPath))
                    {
                        mConn.Open();
                        mCmd = new SQLiteCommand("insert into tbl_item (product, sell_price, quantity, total, server) values(@product, @sell_price, @quantity, @total, @server)", mConn);
                        mCmd.Parameters.AddWithValue("@product", item_product);
                        mCmd.Parameters.AddWithValue("@sell_price", txtsellprice.Text.Replace(".", ","));

                        string quant = txtquantitysell.Text;

                        //item_product = words[1];
                        //item_server = words[4];
                        //item_quantity = words[3];
                        string maxquantity = GetTotalQuantityProduct(item_product);
                        MessageBox.Show(item_product + " max :" + maxquantity );

                       

                        string item_Count = GetPrice(item_product)[1];

                        MessageBox.Show(item_product+" "+ item_Count);

                        double current = Convert.ToDouble(item_Count) + Convert.ToDouble(quant);

                        MessageBox.Show(item_product + " " + current);

                        if (current > Convert.ToDouble(maxquantity))
                        {
                            double f_quantity = Convert.ToDouble(maxquantity) - Convert.ToDouble(item_Count);
                            quant = f_quantity.ToString();
                        }

                       



                        mCmd.Parameters.AddWithValue("@quantity", quant);




                        double total = Convert.ToDouble(txtsellprice.Text.Replace(".", ",")) * Convert.ToDouble(quant);

                        mCmd.Parameters.AddWithValue("@total", total.ToString());
                        mCmd.Parameters.AddWithValue("@server", item_server);
                        mCmd.Prepare();
                        mCmd.ExecuteNonQuery();
                        LoadItemsData();
                    }
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("Error Message : " + ex);
                }
                txtsellprice.Text = String.Empty;
                txtquantitysell.Text = String.Empty;
            }
        }

        private void Button_Delete_ItemElement_Click(object sender, RoutedEventArgs e)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            try
            {
                using (mConn = new SQLiteConnection(mDbPath))
                {
                    mConn.Open();
                    mCmd = new SQLiteCommand("delete from tbl_item where id = " + Int32.Parse(item_id), mConn);
                    mCmd.ExecuteNonQuery();
                    LoadItemsData();
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Error Message : " + ex);
            }
        }

        private void Button_Edit_ItemElement_Click(object sender, RoutedEventArgs e)
        {
            if (item_id != null)
            {
                if (!txtsellprice.Text.Equals(item_sell) || !txtquantitysell.Text.Equals(item_quantity))
                {
                    try
                    {
                        using (mConn = new SQLiteConnection(mDbPath))
                        {
                            mConn.Open();

                            mCmd = new SQLiteCommand("update tbl_item SET product = @product, sell_price = @sell_price, quantity = @quantity, total = @total, server = @server where id =" + Int32.Parse(item_id), mConn);
                            mCmd.Parameters.AddWithValue("@product", item_product);
                            mCmd.Parameters.AddWithValue("@sell_price", txtsellprice.Text.Replace(".", ","));
                            mCmd.Parameters.AddWithValue("@quantity", txtquantitysell.Text);

                            double total = Convert.ToDouble(txtsellprice.Text) * Convert.ToDouble(txtquantitysell.Text);

                            mCmd.Parameters.AddWithValue("@total", total.ToString());

                            //mCmd.Parameters.AddWithValue("@total", item_total);
                            mCmd.Parameters.AddWithValue("@server", item_server);
                            mCmd.ExecuteNonQuery();
                            LoadItemsData();
                        }
                    }
                    catch (Exception ex)
                    {
                       // MessageBox.Show("Error Message : " + ex);
                    }
                }
                else
                {
                   // MessageBox.Show("No Entry Changed.");
                }
            }
        }





        private void itemsDataGridatDatabase_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.itemsDataGridatDatabase.SelectedItem != null)
                {
                    item = this.itemsDataGridatDatabase.SelectedItem;
                    item_id = (this.itemsDataGridatDatabase.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text;
                    item_product = (this.itemsDataGridatDatabase.SelectedCells[1].Column.GetCellContent(item) as TextBlock).Text;
                    item_sell = (this.itemsDataGridatDatabase.SelectedCells[2].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtsellprice.Text = item_sell;
                    item_quantity = (this.itemsDataGridatDatabase.SelectedCells[3].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtquantitysell.Text = item_quantity;
                    item_total = (this.itemsDataGridatDatabase.SelectedCells[4].Column.GetCellContent(item) as TextBlock).Text;
                    item_server = (this.itemsDataGridatDatabase.SelectedCells[5].Column.GetCellContent(item) as TextBlock).Text;
                    ServerList_item.SelectedItem = item_server;
                }
            }
            catch (Exception exp)
            {
               // MessageBox.Show("Message:" + exp);
            }
        }

        #endregion

        #region Servers
        private void LoadServerData()
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            using (mConn = new SQLiteConnection(mDbPath))
            {
                mConn.Open();
                mCmd = null;
                mCmd = new SQLiteCommand("SELECT * FROM tbl_server order by id", mConn);
                mCmd.ExecuteNonQuery();
                mAdapter = new SQLiteDataAdapter(mCmd);
                mTable = new DataTable("tbl_server");
                mAdapter.Fill(mTable);
                mConn.Close();
                categoryDataGridatDatabase_Server.ItemsSource = mTable.DefaultView;
                mAdapter.Update(mTable);
                this.categoryDataGridatDatabase_Server.Columns[0].Header = "ID";
                this.categoryDataGridatDatabase_Server.Columns[1].Header = "SERVER";

            }
        }
        private void categoryDataGridatDatabase_Server_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.categoryDataGridatDatabase_Server.SelectedItem != null)
                {
                    item = this.categoryDataGridatDatabase_Server.SelectedItem;

                    Server_id = (this.categoryDataGridatDatabase_Server.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text;

                    Server_name = (this.categoryDataGridatDatabase_Server.SelectedCells[1].Column.GetCellContent(item) as TextBlock).Text;
                    this.txtServer.Text = Server_name;

                }
            }
            catch (Exception exp)
            {
               // MessageBox.Show("Message:" + exp);
            }
        }
        private void Button_Submit_Server(object sender, RoutedEventArgs e)
        {
            if (txtServer.Text != "" )
            {
                mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
                try
                {
                    using (mConn = new SQLiteConnection(mDbPath))
                    {
                        mConn.Open();
                        mCmd = new SQLiteCommand("insert into tbl_server (Name) values(@Name)", mConn);
                        mCmd.Parameters.AddWithValue("@Name", txtServer.Text);
                        mCmd.ExecuteNonQuery();
                        LoadServerData();
                        PopulateServerList();
                    }
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("Error Message : " + ex);
                }
                txtServer.Text = String.Empty;
                
            }
        }

        private void Button_Delete_ServerElement_Click(object sender, RoutedEventArgs e)
        {
            mDbPath = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.ConnectionStrings["mDbPath"].ConnectionString;
            try
            {
                using (mConn = new SQLiteConnection(mDbPath))
                {
                    mConn.Open();
                    mCmd = new SQLiteCommand("delete from tbl_server where id = " + Int32.Parse(Server_id), mConn);
                    mCmd.ExecuteNonQuery();
                    LoadServerData();
                    PopulateServerList();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error Message : " + ex);
            }
        }

        #endregion


    }
}

